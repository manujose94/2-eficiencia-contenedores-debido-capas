#!/bin/bash
check(){
	if [ $1 != 0 ];then
		echo "[ERROR] Errors occured: $2"
		exit 127
	fi
}
function create_filled(){
    folder_name=$1
    create="$(pwd)/utils/create-files.sh"
    #Delete if folder exist and recreate and refill
    if [ -d "$(pwd)/$folder_name/"  ]; then rm -r $(pwd)/$folder_name; fi
    mkdir -p mkdir $(pwd)/$folder_name $(pwd)/$folder_name/big-files $(pwd)/$folder_name/small-files;

    echo -e "\t [CREATE] big-files and small-files in [$(pwd)/$folder_name] ..."
    OUTPUT=$( source $create "$(pwd)/$folder_name/big-files" "$(pwd)/$folder_name/small-files")
    check $? $OUTPUT
}
create_filled 'test-current-layer'
