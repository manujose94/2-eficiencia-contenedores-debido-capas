#!/bin/bash

dockerfileBase=Dockerfile.base
# Docker file based on base
dockerfileTest1=Dockerfile.test
imgTagBase=manu/mybaseimage:1.0
imgTagTest1=manu/test1:1.0
#Volumen with same data that containers big-files and small-files
nameVolumen=data_test
#Containers names
name0=Base
name1=Test1

# Default: If Image not exist then it will be created
# 1 param: stop = stop current docker containers
#		   delete=stop containers and delete images and data_test volumen
#

checkerror() {
	RESULT=$1
	if [ $RESULT != 0 ];then
		echo "[ERROR] Errors occured while launching image dockers: $2"
		exit 127
	fi
}
stop-and-clear(){
for i in $name0 $name1; do
	CONTAINER=${i}
	matchingStarted=$(docker ps --filter="name=$CONTAINER" -q | xargs)
	if [[ -n $matchingStarted ]]; then
		docker stop $matchingStarted		
		echo "$NAME_CONTAINER$i container running then stopped"
	fi
	matching=$(docker ps -a --filter="name=$CONTAINER" -q | xargs)
	if [[ -n $matching ]]; then
		docker rm $matching		
		echo "$NAME_CONTAINER$i container stopped then remove it"
	fi	
done
#Delete results of docker
docker volume rm myresults
if [[ $1 == *delete*  ]];then
for img in $imgTagTest1 $imgTagBase; do
	docker rmi $(docker images -q --filter=reference="$img") &> /dev/null
	docker volume rm $nameVolumen &> /dev/null
done
fi
if [[ $1 == *stop* || $1 == *delete*  ]]; then	echo 'exit'; exit 0;
fi
}
#
# === MAIN ===
#

if [ $(id -u) -ne 0 ]; then
	echo "[WARN] Run this script as a Root user only" >&2
	exit 1
fi
#----------------------------------
# Stop and Clear
# --------------------------------
stop-and-clear $1 
echo '[Delete] Delete current container wether it exist'
#----------------------------------
# Image Base
# --------------------------------
echo '[Create] Images if it not exist..'
if [[ "$(docker images -q $imgTagBase 2> /dev/null)" == "" ]]; then
	ts=$(date +%s); 
	# steps by time: | grep "^Step" | while read line ; do echo "$(date +%s%N)| $line"; done;
	docker build -t $imgTagBase -f $dockerfileBase . &> /dev/null
	checkerror $?
	tt=$((($(date +%s) - $ts))) ; 
	echo "[CREATED] Image: $imgTagBase -Time taken: $tt seconds"
fi
#----------------------------------
# Image Test1 Based on Image Base
# --------------------------------
if [[ "$(docker images -q $imgTagTest1 2> /dev/null)" == "" ]]; then
	ts=$(date +%s); 
	docker build -t $imgTagTest1 -f $dockerfileTest1 . &> /dev/null
	checkerror $?
	tt=$((($(date +%s) - $ts))) ; 
	echo "[CREATED] Image: $imgTagTest1 -Time taken: $tt seconds"
fi
#----------------------------------
# Create Volumen for "Test Bind Mount"
# --------------------------------
#"/var/lib/docker/volumes/$nameVolumen/_data"
echo '[Create] Create the volumen and data (simulate host folder)'
OUTPUT=$( source "$(pwd)/scripts/utils/create-and-fill-volumen.sh")
checkerror $? $OUTPUT
#----------------------------------
# Create Volumen to share results generated within Container
# --------------------------------
echo '[Create] Create the volumen for results [myresults]'
docker volume create myresults &> /dev/null
mkdir -p /var/lib/docker/volumes/myresults/_data/base-image
mkdir -p /var/lib/docker/volumes/myresults/_data/image-1
checkerror $? $OUTPUT
#----------------------------------
# Launch Docker Container for Tests
# --------------------------------
if [[ $1 == *start*  ]];then
echo '[RUN] Container Test 1'
docker run  --rm -t -d \
	--privileged \
   -v $nameVolumen:/usr/src/app/myvolumen \
   -v /var/lib/docker/volumes/myresults/_data/image-2:/usr/src/app/results \
   --name=$name1 $imgTagTest1 bash
checkerror $?
fi

#docker run  --rm -t -d \
#	--privileged \
#	-v mytest:/usr/local/result/ \
#	--name=test-lesslayers manu/mybaseimage:1.0 bash
