#!/usr/bin/env python3
import os
import sys

import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure

from test import NUM_INSTANCES
from test import STORAGE_DRIVERS
from test import all_tests

LAYER = ('0', '1', '5', '10')
TYPE_TEST=('throughput', 'latency')
def all_test_types():
    return {
        '.'.join(f.strip('.').split('.')[0:1])
        for f in os.listdir('results')
        if not f.startswith('.')
    }


def average_from_file(path):
    try:
        with open(path) as f:
            lines = [float(line) for line in f.readlines()]
        return min(lines)
    except FileNotFoundError:
        return 0


def main(argv=None):
    width = 1
    label_format = '{:,.0f}'
    fig = Figure(figsize=(10, 5))
    FigureCanvasAgg(fig)  # why do i have to do this?
    ax = fig.add_subplot(1, 1, 1)
    x = np.array([float(i)*(len(LAYER)+1) for i in range(len(TYPE_TEST))])
    bars = []

    for type_file, color in zip(LAYER, 'mbcgy'):
        means = []
        for type_test in TYPE_TEST:
                means.append(average_from_file(os.path.join(
                    'dd-test-results',
                    '{}-{}'.format(type_test,type_file),
                )))
        print (means)

        bars.append(ax.bar(x, means, width, color=color))
        x += width

    ax.set_title('1 Instance Overlay2')
    ax.set_xlabel('Type of test')  # lol
    ax.set_ylabel('seconds for completion')
    ax.set_xticklabels(TYPE_TEST,rotation="horizontal", size=10)
    ax.set_xticks(x - 1.5)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1,box.width, box.height * 0.9])
    
    ax.legend((bar[0] for bar in bars), LAYER, loc='upper center',
            bbox_to_anchor=(0.5, -0.05),
            fancybox=True, shadow=True, ncol=5)

    fig.savefig(os.path.join('graphs', 'mygraphlayer' + '.png'))


if __name__ == '__main__':
    sys.exit(main())
